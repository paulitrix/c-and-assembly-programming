/*
	Title: replace.c
	
	Description: This program replaces spaces with minus signs and 
		     delivers the number of spaces it has replaced.

	Author: Paul N Muchene

	Date: 8/12/2009 

*/

#include <stdio.h> // Standard i/o
#include <string.h> // String function

/*Replace function*/

void replace(char buff[])
{
	int count=0;
	char *y;

	y=buff;


	do
	{
		if(*y==' ')
		{
		
			*y='-';
			
			count+=1;
		}

		y++;

	}while(*y);	
	

	printf("%s\n",buff);

	printf("\nNumber of spaces replaced:\t %d\n",count);

}

/*Main program starts here*/
int main()
{
	char ch,buff[256];
	int loop=0;

	printf("\nEnter some text:\n"); // Prompt user input


	while((ch=getchar())>=0)
	{
		buff[loop]=ch;
		loop++;

	}

	strcat(buff,"\0");

	replace(buff); // Call replace function
	

}/* End of program*/
