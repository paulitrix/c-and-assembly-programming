/*

	Title: paytime.c

	Description: This program reads in two integers representing rate of pay		      and number of hours. It then prints out the total pay, 
		     with hours up to 40 hours being paid at basic rate, from
		     40-60 at rate and a half, above 60 at double rate. 
		     The total pay as pounds is printed to two decimal places.
		
		     This program makes use of random numbers. 

	Author: Paul N Muchene

	Compilation: cc -o paytime paytime.c

	Date: 08-02-2010


*/

//Precompiler routines
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define basic 40
#define max 60

/*The main payment function*/
void paytime()
{
	int hour,pay,i=0;
	float total=0,sum;

	srand((unsigned int) time (NULL)); //Seed our random numbers

	do
	{
		hour=rand()%100;
		pay=rand()%300;

		if(hour<basic) /*Pay basic rate if hours<40 */
		{
			sum=(pay/100.00)*hour;	
		}
		if(hour>basic && hour<max) /*Pay 1.5 times basic rate if 40<hour<60 */
		{
			sum=(1.5*pay/100.00)*hour;	
		}
		if(hour>max) /*Pay double if hours>40*/
		{
			sum=(2*pay/100.00)*hour;	
		}
		if(pay==0) /*Print total and quit if pay==0*/
		{
			printf("\nTotal pay is:\t%.2f pounds\n",total);
			printf("\nIterations:\t%d\n",i);
			exit(0);
		}
		
		i++;

		total+=sum; /*Total payment*/

		printf("\nPay at %d pence/hr for %d hours is %.2f pounds\n",pay,hour,sum );

	}while(pay>0);


}

/*Main program starts here*/
int main()
{
	
	paytime();

	return 0;

}/*End*/
