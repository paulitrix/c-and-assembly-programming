/*
	Title: diamond.c

	Description: Draws a diamond on the screen

	Author: Paul N Muchene

	Date: 08-02-2010

*/

#include<stdio.h>
#include<math.h>

int main()
{
	int i,j,number,loop,modulus;
	char ch='*';
	
	printf("\nEnter an integer:\t");
	scanf("%d",&number);
	
	number=number/2+1;

	for(i=0;i<number;i++)
	{

		for(loop=number;loop>i;loop--)
		{
			printf(" ");

		}
		for(j=0;j<i;j++)
		{
			printf("%c",ch);
			printf("%c",ch);
		}
		printf("%c\n",ch);

	}

	for(i=0;i<number-1;i++)
	{
		for(loop=0;loop<i+2;loop++)
		{
			printf(" ");

		}
		for(j=number-2;j>i;j--)
		{
			printf("%c",ch);
			printf("%c",ch);
		}

		printf("%c\n",ch);
	}

	printf("\n");
	return 0; 

}
