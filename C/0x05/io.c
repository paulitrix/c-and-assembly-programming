/*
	Title: io.c
	
	Description: This program reads a file and counts:
		
			1. The number of lines 
			2. The number of blank lines
			3. The total number of characters
			4. The total number of spaces after a tab
			5. The total number of leading spaces after tab
			6. The total number of comments
			7. The total number of characters in the comments
			8. Total number of identifiers and charcters in them
			9. Total number of indentations

	Compilation: cc -o io io.c

	Author: Paul N Muchene

	Date: 12-01-2010

*/

// Precompiler routines
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<ctype.h>

/* Global variables */ 
#define MAX 6000

FILE *fopen(),*instream;

int lines=0,blanks=0,spaces=0,tabs=0,tabspaces=0,leading=0;

/* Function to count the number of comments */ 
int comments(char bluff[])
{
	char *i=bluff,*j;
	int x=0,count=0;

		while(x<strlen(bluff)-1)
		{
			j=i+1;

			if(*i=='/' && *j=='/')
			{

				count+=1;
			}
			if(*i=='/' && *j=='*')
			{
				count+=1;
			}

			if(*i=='*' && *j=='/')
			{
				count+=1;
			}
			x++;
			i++;
		}

	return count;

}

/* Function to count the number of characters within the comments */
int countchars(char bluff[])
{
	char *i=bluff,*j,*m,*n;
	int x=0,count=0,total=0;

		while(x<strlen(bluff)-1)
		{
			j=i+1;

			if(*i=='/' && *j=='*')
			{
			   m=i+1;
			   n=j+1;

				do	
				{
					count++;
					n++;
					m++;

				}while(*m!='*'&&*n!='/');	

			}
			x++;
			i++;
		}

	return count;
}

/* Function to count the number of tabs, spaces after tab and leading spaces*/
int tabchars(char bluff[])
{
	char *i=bluff;
	char *m,*n;
	int x=0,count=0,total=0;

		while(x<strlen(bluff)-1)
		{
			if(*i=='\t')
			{
				m=i+1;
				if(*i=='\t' && *m==' '){leading++;}
				if(*m=='\t'){m++;}
				else
				{
					while(!iscntrl(*m))
					{
						if(*m==' '){tabspaces++;}
						count++;
						m++;
					}

				}
				
			}
			i++;
			x++;
		}	


	return count; 
}

/* Function to find out the total number of identifiers */
int identifiers(char bluff[])
{
	char *i=bluff,*j,*m;
	int x=0,count=0,occur=0; 

	while (x<strlen(bluff)-1)
	{
		j=i+1;
		
		if(*i=='/' && *j=='*')
		{
			i++;	
		}
		if(*i=='*' && *j=='/')
		{
			i++;
		}
		if(*i=='/' && *j=='/')
		{
			i++;

		}
		if(*i=='"'|| iscntrl(*i)) 
		{
			i++;
		}
		else
		{
			if(isspace(*i)){occur++;i++;}
			else{count++;}
		}
		
		i++;
		x++;
	}

	return occur;
}

/* Function to find out total identation and identation errors */
int identation(char bluff[])
{
	char *i=bluff;
	int x=0,count=0;

	while(x<strlen(bluff)-1)
	{
		if(*i=='{' || *i=='}'){count++;}
		i++;
		x++;
	}

	return count;
}

/* Function to count number of lines,blanks,spaces,tabs and returns %of blank lines 
*/ 
void getline(char bluff[])
{
	char *i=bluff,*j,lastchar='\0';
	int x=0;

		while(x<strlen(bluff))
		{
			j=i+1;

			switch(*i)
			{
				case '\n':

					lastchar=*i;
					lines++;

					if(lastchar!='\n'){lines++;}
				
					if(*j==' '){blanks++;}

				break;

				case ' ':
					spaces++;
				break;

				case '\t':
					tabs++;	
				break;
				
			}

			x++;
			i++;	
		}

}

/* Main program starts here */ 
main()
{
	char ch,buff[MAX];
	int loop=0;
	float percentblank,avgcharline,avglcharline,avgleading,avgspaces,\
	      percomment,perchar;
	instream=fopen("code.txt","r"); // Open code.txt for read only
	
	if(instream==NULL) // Can't find file 
	{
		fprintf(stderr,"\n File does not exist\n");
		exit(1);
	}

	else
	{
		while((ch=getc(instream))!=EOF) // Place file contents into buff
		{
			buff[loop]=ch;
			loop++;
		}

		getline(buff); // getline function 
		countchars(buff);// countchars function
		comments(buff); // comments function
		tabchars(buff); // tabchars function
		identifiers(buff); // identifiers function
		identation(buff); // identation function 
		
	}

	/* Statistics */ 

	percentblank=(blanks/(float)lines)*100;
	avgcharline= loop/(float)lines;
	avglcharline=(loop-leading)/(float)lines;
	avgleading=leading/(float)lines;
	avgspaces=((spaces-leading)/(float)lines);
	perchar=(countchars(buff)/(float)loop)*100;
	percomment=(comments(buff)/(float)lines)*100;


	/* Display analysis to  stdout */ 

	printf("\nTotal lines:\t%d\n",lines);
	printf("\nTotal  blank lines:\t%d\n",blanks);
	printf("\nPercentage of  blank lines:\t%.2f\n",percentblank);
	printf("\nTotal  characters:\t%d\n",loop);
	printf("\nTotal  spaces:\t%d\n",spaces);
	printf("\nTotal  tabs:\t%d\n",tabs);
	printf("\nTotal  characters after tabs:\t%d\n",tabchars(buff));
	printf("\nTotal spaces after tab:\t%d\n",tabspaces);
	printf("\nTotal leading spaces:\t%d\n",leading);
	printf("\nAverage characters per line :\t%.2f\n",avgcharline);
	printf("\nAverage characters per line ignoring leading spaces :\t%.2f\n",avglcharline);
	printf("\nAverage leading spaces per line :\t%.4f\n",avgleading);
	printf("\nAverage spaces per line ignoring leading:\t%.2f\n",avgspaces);
	printf("\nTotal  comments:\t%d\n",comments(buff));
	printf("\nTotal characters in comments:\t%d\n",countchars(buff));
	printf("\nPercentage number of comments to total lines:\t%.2f\n",percomment);
	printf("\nPercentage number of characters in comments to characters:\t%.2f\n",perchar);
	printf("\nTotal characters in identifiers :\t%d\n",identifiers(buff));
	printf("\nTotal identation occurrences:\t%d\n",identation(buff));
	if(identation(buff)==0){printf("\nNo identatation in file\n");}
	if(identation(buff)%2==0){printf("\nIdentation OK!\n");}
	else{printf("\nYou have identation errors\n");}

	fclose(instream); // Close stream

	return 0; 

}/* End of program */
