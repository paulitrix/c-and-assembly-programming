/*
	Title: diamond.c

	Description: Draws a multiplication table on screen

	Author: Paul N Muchene

	Date: 08-02-2010

*/

#include<stdio.h>
#include<math.h>

int main()
{
	int i,j,number,loop,modulus;
	char ch='*';
	
	printf("\nEnter an integer:\t");
	scanf("%d",&number);
	
	printf("\n");

	for(i=0;i<=number;i++)
	{
		printf("%d",i);
		printf("\t");

	}
	for(i=0;i<=number;i++)
	{
		printf("\n%d",i);

		for(j=1;j<=i;j++)
		{
			printf("\t%d",j*i);
		}

	}

	printf("\n");

	return 0; 

}
