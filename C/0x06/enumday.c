/*
	Name: enumday.c

	Description: Prints out e following day using enumerated types.

	Auor: Paul N Muchene

	Date: 13-02-2010

*/

//Precompiler libraries
#include<stdio.h>
#include<stdlib.h>
#include<math.h>


char	 *try,*jan="January",*feb="February",*mar="March"\
    	,*apr="April",*may="May",*jun="Jun",*jul="Jul"\
    	,*aug="August",*sep="September",*oct="October"\
    	,*nov="November",*dec="December";

enum month {January=1,February,March,April,May,June,July,\
	    August,September,October,November,December
	   } this_month;

int day,cal,kesho;

int main()
{

	printf("\nEnter day (1-31):\t");
	scanf("%d",&day);

	printf("\nEnter month (1-12):\t");
	scanf("%d",&cal);

	if(day>31||day<=0){printf("\n Unrecognized day \n"); exit(1);}
	if(cal>12||cal<=0){printf("\n Month is outside range \n"); exit(1);}

	this_month=cal;

	switch(this_month)
	{
		case 1:
			try=jan;
			kesho=(day+1)%32;
			if(kesho==0)
			{
			kesho=1; 
			try=feb;
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==21||kesho==31)
			{
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==2||kesho==22)
			{
			printf("\nFollowing Day: %dnd %s\n",kesho,try);
			}
			else if(kesho==3||kesho==23)
			{
			printf("\nFollowing Day: %drd %s\n",kesho,try);
			}
			else	
			{
			printf("\nFollowing Day: %dth %s\n",kesho,try);
			}
		break;

		case 2:
			try=feb;
			kesho=(day+1)%30;
			if(kesho==0)
			{
			kesho=1; 
			try=mar;
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==21)
			{
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==2||kesho==22)
			{
			printf("\nFollowing Day: %dnd %s\n",kesho,try);
			}
			else if(kesho==3||kesho==23)
			{
			printf("\nFollowing Day: %drd %s\n",kesho,try);
			}
			else	
			{
			printf("\nFollowing Day: %dth %s\n",kesho,try);
			}
		break;

		case 3:
			try=mar;
			kesho=(day+1)%32;
			if(kesho==0)
			{
			kesho=1; 
			try=apr;
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==21||kesho==31)
			{
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==2||kesho==22)
			{
			printf("\nFollowing Day: %dnd %s\n",kesho,try);
			}
			else if(kesho==3||kesho==23)
			{
			printf("\nFollowing Day: %drd %s\n",kesho,try);
			}
			else	
			{
			printf("\nFollowing Day: %dth %s\n",kesho,try);
			}
		break;

		case 4:
			try=apr;
			kesho=(day+1)%31;
			if(kesho==0)
			{
			kesho=1; 
			try=may;
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==21)
			{
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==2||kesho==22)
			{
			printf("\nFollowing Day: %dnd %s\n",kesho,try);
			}
			else if(kesho==3||kesho==23)
			{
			printf("\nFollowing Day: %drd %s\n",kesho,try);
			}
			else	
			{
			printf("\nFollowing Day: %dth %s\n",kesho,try);
			}
		break;
		case 5:
			try=may;
			kesho=(day+1)%32;
			if(kesho==0)
			{
			kesho=1; 
			try=jun;
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==21||kesho==31)
			{
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==2||kesho==22)
			{
			printf("\nFollowing Day: %dnd %s\n",kesho,try);
			}
			else if(kesho==3||kesho==23)
			{
			printf("\nFollowing Day: %drd %s\n",kesho,try);
			}
			else	
			{
			printf("\nFollowing Day: %dth %s\n",kesho,try);
			}
		break;

		case 6:
			try=jun;
			kesho=(day+1)%31;
			if(kesho==0)
			{
			kesho=1; 
			try=jul;
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==21)
			{
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==2||kesho==22)
			{
			printf("\nFollowing Day: %dnd %s\n",kesho,try);
			}
			else if(kesho==3||kesho==23)
			{
			printf("\nFollowing Day: %drd %s\n",kesho,try);
			}
			else	
			{
			printf("\nFollowing Day: %dth %s\n",kesho,try);
			}
		break;

		case 7:
			try=jul;
			kesho=(day+1)%32;
			if(kesho==0)
			{
			kesho=1; 
			try=aug;
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==21||kesho==31)
			{
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==2||kesho==22)
			{
			printf("\nFollowing Day: %dnd %s\n",kesho,try);
			}
			else if(kesho==3||kesho==23)
			{
			printf("\nFollowing Day: %drd %s\n",kesho,try);
			}
			else	
			{
			printf("\nFollowing Day: %dth %s\n",kesho,try);
			}

		break;

		case 8:
			try=aug;
			kesho=(day+1)%32;
			if(kesho==0)
			{
			kesho=1; 
			try=sep;
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==21||kesho==31)
			{
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==2||kesho==22)
			{
			printf("\nFollowing Day: %dnd %s\n",kesho,try);
			}
			else if(kesho==3||kesho==23)
			{
			printf("\nFollowing Day: %drd %s\n",kesho,try);
			}
			else	
			{
			printf("\nFollowing Day: %dth %s\n",kesho,try);
			}
		break;

		case 9:
			try=sep;
			kesho=(day+1)%31;
			if(kesho==0)
			{
			kesho=1; 
			try=oct;
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==21)
			{
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==2||kesho==22)
			{
			printf("\nFollowing Day: %dnd %s\n",kesho,try);
			}
			else if(kesho==3||kesho==23)
			{
			printf("\nFollowing Day: %drd %s\n",kesho,try);
			}
			else	
			{
			printf("\nFollowing Day: %dth %s\n",kesho,try);
			}
		break;

		case 10:
			try=oct;
			kesho=(day+1)%32;
			if(kesho==0)
			{
			kesho=1; 
			try=nov;
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==21||kesho==31)
			{
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==2||kesho==22)
			{
			printf("\nFollowing Day: %dnd %s\n",kesho,try);
			}
			else if(kesho==3||kesho==23)
			{
			printf("\nFollowing Day: %drd %s\n",kesho,try);
			}
			else	
			{
			printf("\nFollowing Day: %dth %s\n",kesho,try);
			}

		break;

		case 11:
			try=nov;
			kesho=(day+1)%31;
			if(kesho==0)
			{
			kesho=1; 
			try=dec;
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==21)
			{
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==2||kesho==22)
			{
			printf("\nFollowing Day: %dnd %s\n",kesho,try);
			}
			else if(kesho==3||kesho==23)
			{
			printf("\nFollowing Day: %drd %s\n",kesho,try);
			}
			else	
			{
			printf("\nFollowing Day: %dth %s\n",kesho,try);
			}
		break;

		case 12:
			try=dec;
			kesho=(day+1)%32;
			if(kesho==0)
			{
			kesho=1; 
			try=jan;
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==21||kesho==31)
			{
			printf("\nFollowing Day: %dst %s\n",kesho,try);
			}
			else if(kesho==2||kesho==22)
			{
			printf("\nFollowing Day: %dnd %s\n",kesho,try);
			}
			else if(kesho==3||kesho==23)
			{
			printf("\nFollowing Day: %drd %s\n",kesho,try);
			}
			else	
			{
			printf("\nFollowing Day: %dth %s\n",kesho,try);
			}

		break;
	}
	

	return 0;
}
