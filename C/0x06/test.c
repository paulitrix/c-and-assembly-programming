/* # This is a test file for file writing routines */

#include<stdio.h>
#include<stdlib.h>

FILE *fopen(),*outstream;

int loop=0,ch;

char buff[100],*file="personal.txt";

// Main program begins here
int main()
{
	outstream=fopen("personal.txt","w");
	
	if(outstream==NULL){ fprintf(stderr,"Can't open file"); exit(-1);}
	else
	{
		printf("\nEnter some text:\t");
		while((ch=getchar())>=0)
		{
			buff[loop]=ch;
			loop++;

		}

		fprintf(outstream,buff);


	}
	fclose(outstream);
	return 0;
}
