/*
	Title: uniondb.c

	Description: This is a simple program to store a persons details in a 
			file using unions.

	Author: Paul N Muchene

	Date: 18-02-2010

*/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#define MAX 100 

char ch,buff[MAX],buff1[MAX],buff3[MAX],buff4[MAX],buff5[MAX],buff6[MAX];
FILE *outstream,*fopen();

union personal{
	char *fname;
	char *sname;
	char *age;
	char *dob;
	char *address;
	char *place;
	char *country;	
}ppl;

/* Main program starts here */
int main()
{
	int loop=0;

	outstream=fopen("personal.txt","a");
	
	if(outstream==NULL){fprintf(stderr,"Can't open file"); exit(-1);}

	else{

		printf("\nEnter your first name:\t");
		while((ch=getchar())!='\n')
		{
			buff[loop]=ch;
			loop++;
		}
		ppl.fname=strcat(buff,"\t");
		fprintf(outstream,ppl.fname);
		loop=0;

		printf("\nEnter your surname:\t");
		while((ch=getchar())!='\n')
		{
			buff1[loop]=ch;
			loop++;
		}

		ppl.sname=strcat(buff1,"\t");
		fprintf(outstream,ppl.sname);
		loop=0;


		printf("\nEnter your date of Birth (dd/mm/yyyy):\t");
		while((ch=getchar())!='\n')
		{
			buff3[loop]=ch;
			loop++;
		}

		ppl.dob=strcat(buff3,"\t");
		fprintf(outstream,ppl.dob);
		loop=0;

		printf("\nEnter your address:\t");
		while((ch=getchar())!='\n')
		{
			buff4[loop]=ch;
			loop++;
		}

		ppl.address=strcat(buff4,"\t");
		fprintf(outstream,ppl.address);
		loop=0;

		printf("\nEnter your city:\t");
		while((ch=getchar())!='\n')
		{
			buff5[loop]=ch;
			loop++;
		}
		ppl.place=strcat(buff5,"\t");
		fprintf(outstream,ppl.place);
		loop=0;

		printf("\nEnter your country:\t");
		while((ch=getchar())!='\n')
		{
			buff6[loop]=ch;
			loop++;
		}

		ppl.country=strcat(buff6,"\n");
		fprintf(outstream,ppl.country);
		loop=0;

	}

	fclose(outstream);

	return 0;

}/* Program ends */
