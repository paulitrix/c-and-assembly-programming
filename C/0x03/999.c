/*

Title: 999.c

Description: This program reads in numbers until -999 is encountered. The sum
	     of all numbers is then displayed on encountering -999

Compilation: cc -o 999 999.c

Execution: ./999

Author: Paul N Muchene

*/

#include<stdio.h> //Standard i/o library
#include<stdlib.h> //Standard utility library

/*Program starts*/
int main()
{
 	
	int sum=0,num;

	printf("\n Enter numbers input -999 to exit\n");
	while(scanf("%d",&num))
	{
		sum=sum+num;	
	
		if(num==-999){break;}
			
	}

	printf("The sum of your numbers is: %d\n",sum);

}/*Program ends*/
