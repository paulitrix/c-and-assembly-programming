/*

Title: interest.c

Description: This program reads in an interest rate, an amount and a duration
	     and calculates the interest and sum as type float

Compilation: cc -o interest interest.c

Execution: ./interest 

Author: Paul N Muchene


*/

#include<stdio.h>
#include<stdlib.h>

/*Program starts here*/
int main()
{
	int years,principal,loop;
	float interest,rate,capital;
	
	printf("\nEnter your principal (in pence):\t");
	scanf("%d",&principal);
	printf("\nEnter your interest rate:\t");
	scanf("%f",&rate);
	printf("\nEnter your duration (years):\t");
	scanf("%d",&years);

	if(principal<0){printf("\nError: Negative values disallowed\n"); exit(0);}

	capital=(float)principal;
	
	printf("Original sum: %2f at %f percent for %d years\n",capital,rate,years);

	printf("\n\nYear\tInterest\tSum\n");
	printf("\n+-------------+---------------+----\n");

	for(loop=1;loop<=years; loop++)
	{
		interest=(float)(capital*(rate/100.0));
	
		capital+=interest;

		printf("%d	%2f%	%2f\n", loop,interest,capital);
	}
	return 0;

}/*Program ends*/
