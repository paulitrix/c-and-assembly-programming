/*

Title: 1value.c

Description: This porgram reads in a positive integer and computes the following sequence.
		a) If the number is even, it is halved
		b) If the number is odd, it is mulitiplied by 3 and 1 added to it
	      This process is repeated until the final value for our positive integer is 1

Compilation: cc -o 1value 1value.c

Execution: ./1value

Author: Paul N Muchene


*/

#include<stdio.h> //Standard i/o library
#include<stdlib.h> //Standard utility library

/* Program starts here*/
int main()
{
	int number,loop=0,check,value;

	printf("Enter your number:\t");
	scanf("%d",&number);

	printf("\nInitial value is: %d\n",number);

	do
	{
		check=number%2;

		if(check==0)
		{
			value=number/2;
		}
		else
		{
			value=1+(number*3);
		}

		number=value;
			
		printf("\nNext value is:\t%d\n",value);	


		loop++;

		if(value==1){break;}

	}while(value>=1);


	printf("\nFinal value is %d, number of steps is %d\n",value,loop);

	return 0;

}/*Program ends here*/
