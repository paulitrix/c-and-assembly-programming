/*

Title: englishtext.c

Description: This program reads a file of english text, and prints it out
	     one word per line, all punctuation and non-alpha characters
	     being omitted.

Compilation: cc -o englishtext englistext.c

Execution: ./englishtext

Author: Paul N Muchene

*/

#include<stdio.h> //Standard i/o library
#include<string.h> //String library
#include<ctype.h> //String testing library

FILE *stream, *fopen();
char buff[256],string[256],*word,*check;
int ch,loop=0,i=0;


/* Program begins*/
int main()
{
	stream=fopen("myfile.txt","r");

	if(stream==NULL)
	{
		printf("\nCan't open file\n");
	}
	else
	{
		while((ch=getc(stream))!=EOF)
		{


			if(isdigit(ch)||iscntrl(ch)||ispunct(ch))
			{
				ch=' ';
			}

			string[loop]=ch;

			strcpy(buff,string);

			loop++;

		}

		fclose(stream);

		word=buff;	

		for(check=strtok(word," "); check!=NULL; check=strtok(NULL," "))
		{
			printf("%s \n",check);
		}
	}

	return 0;

}/* Program ends*/
