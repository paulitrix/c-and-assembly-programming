/*

Title: vowels.c

Description: This porgram counts the number of vowesls and letters in free text
	     given as standard input. It then prints out the number of occurences
	     of each of the vowels and displays these occurences as a percentage.

Compilation: cc -o vowels vowels.c

Execution: ./vowels

Author: Paul N Muchene


*/

#include<stdio.h> //Standard library

#include<stdlib.h> //Standard utility library

#include<string.h> //String library


/*Program starts*/
int main()
{
	char ch; //char variable
	int loop=0,loopa=0,loope=0,loopi=0,loopo=0,loopu=0,rest; //integer variables and loops
	float pera,pere,peri,pero,peru,perest; //For percentages

	printf("\nEnter your text (Terminate with ctrl+D)\n");
	while((ch=getchar())>=0)
	{
		if(ch=='a'){loopa++;}
		if(ch=='e'){loope++;}
		if(ch=='i'){loopi++;}
		if(ch=='o'){loopo++;}
		if(ch=='u'){loopu++;}
		loop++;
	}

	rest=loop-(loopa+loope+loopi+loopo+loopu); //Non-vowel count

	/*Percentages*/
	pera=(loopa/(float)loop)*100;
	pere=(loope/(float)loop)*100;
	peri=(loopi/(float)loop)*100;
	pero=(loopo/(float)loop)*100;
	peru=(loopu/(float)loop)*100;
	perest=(rest/(float)loop)*100;

	/*Display results*/
	printf("\nNumber of characters: a: %d e: %d i: %d  o: %d u: %d  rest: %d total: %d\n",loopa,loope,loopi,loopo,loopu,rest,loop);

	printf("\nPercentage of total: a: %.2f%; e: %.2f%; i: %.2f%; o: %.2f%; u: %.2f%; rest: %.2f%; \n",pera,pere,peri,pero,peru,perest);

	return 0;

}/*Program ends*/
