/*

Title: 10numbers.c

Description: This program reads in 10 numbers from stdin and computes their respective
	     average, minimum and maximum values.

Compilation: cc -o 10numbers 10numbers.c (make sure gcc is installed)

Exectution: ./10numbers  (use a (ba|c|tc|k|)sh shell for this )

Author: Paul N Muchene

*/


#include<stdio.h> //Standard i/o library
#include<math.h> //Math library
#include<stdlib.h> //Standard utility library

const int limit=10;

void sort(int numarray[],int limit)
{

	int i,j,temp;

	for(i=0;i<limit-1;i++)
	{
		for(j=i+1;j<limit;j++)
		{
			if(numarray[i]>numarray[j])
			{
				temp=numarray[j];
				numarray[j]=numarray[i];
				numarray[i]=temp;
			}
		}

	}


}


void menu()
{

	int i,num[limit],sum=0,option=1;
	float mean;
	char choice;

	do
	{
		printf("\nEnter 10 numbers\n");
		
		
		for(i=0; i<limit; i++)
		{
			scanf("%d",&num[i]);
			sum=sum+num[i];
		}
		
		mean=(float)sum/10.0;
		sort(num,limit);	
		
		printf("\n Average: %.2f  Min: %d  Max: %d\n",mean,num[0],num[limit-1]);

		printf("\nDo you want to go back to main menu?:\t");
		scanf("%s",&choice);

		switch(choice)
		{
			case 'y':
				option=1;
			break;

			default:
				printf("\nThankyou for using this program\n\n");
				option=0;
			break;
		}

	}while(option==1);

}


/* Main progam starts here */
int main()
{
	menu();
	return 0; //Return from function

}/* Program ends here*/
