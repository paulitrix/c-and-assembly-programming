/*

Title: sortarray.c

Description: This program reads 10 integers from standard input and sorts them
displaying the maximum and minimum value.

Compilation: cc -o sortarray sortarray.c

Author: Paul N Muchene	

*/

#include <stdio.h>


/* Sort function */
void  sort (int numarray[],int limit)
{
	int i,j,temp; //looping variables, temp
	
		for(i=0;i<limit-1;i++) //0<=i<9

		{
			for(j=i+1;j<limit;j++) //i+1<=j<10
			{
				if(numarray[i]>numarray[j]) //actual sort
				{
					temp=numarray[i]; 
					numarray[i]=numarray[j];
					numarray[j]=temp;	
				}
			}

		}


}

/* Program starts here */
int main()
{
	int i,n,numarray[10]; //looping variables, array of integers
	const int limit=10;

	printf("\n Enter 10 integers:\n"); //Prompt user to enter 10 integers



	for(i=0; i<10; i++)
	{
		scanf("%d",&numarray[i]); // read
	}

	sort(numarray,limit); //Call sort function 
	
	
	printf("Smallest value: %d Largest value: %d\n",numarray[0],numarray[9]); // Minimum & Max value 



	return 0;

}/* Program ends here*/
