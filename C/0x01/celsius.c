/*
Title:	This is a program that reads a temperature value in celsius and 
	converts it to farenheit
	
Compilation: cc -o celsius celsius.c
*/

#include <stdio.h> //Standard input/output library

float celsius; //Celsius variable as float

float temperature; // Temperature as float

/* Main program starts here*/
int main()
{
	printf("\n Enter the temperature in celsius:\t"); //Prompt user to enter temperature in celsius

	scanf("%f",&celsius); // read the temperature

	temperature= ((9/5.0)*celsius)+32.0; // Conduct the conversion to farenheit

	printf("\n Your temperature in farenheit is:\t%.2f\n",temperature); // Display the result

	return 0; // Return 
}/* Program ends*/

