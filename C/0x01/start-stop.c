/*

Title: start-stop.c

Description: This program reads in 2 inputs a start time and a duration both of		     
	    type integer and displays the end time 

Compilation: cc -o start-stop start-stop.c

Author: Paul N Muchene

*/

#include<stdio.h> // Standard i/o library
#include<math.h> // Math library

/* Main program starts here*/
int main()
{
	int startime,duration,temp,minute,sum,day; // Our variables

	printf("\nEnter start time:\t"); //Prompt for start time
	scanf("%d",&startime); // Read start-time

	printf("\nEnter duration:\t"); //Prompt for duration
	scanf("%d",&duration); // Read duration

	sum=startime+duration; // Add time and duration

	temp=((sum)/100)*100; // Round off sum to the nearest whole integer

	minute=sum-temp; // Obtain the minutes from the sum entered 

	/* Extract the minutes*/
	if(minute>60)
	{
		sum=100+temp+(minute%60); // Add 1 hour to the time as well as the minutes

	}

	/* Extract the days*/
	if((sum/2400)>=1)
	{
		day=(sum/2400); // Obtain the day
		sum=sum-((sum/2400)*2400); //Correct number of hours and minutes

	}
	else {day=0;} // day=0

	printf("\nStart time is %dh. Duration is %d. End time is day: %d  time: %dh.\n\n",startime,duration,day,sum); //Display results


	return 0; //Function returns a value

}/* Program ends */

