/*
	identity.c:
		A struct program where a user inputs his/her name, address and age
		The result is printed out in stdout

	Compilation: cc -o identitiy identity.c

*/

#include <stdio.h> //Standard input output
#include <string.h> //String library

struct identity{ // Define a struct identity which stores the name, address and age 
	char name[100]; //Name
	char address[100]; //Address
	int age; //Age
};

/* Our program begins here*/
main()
{
	struct  identity id; //Call an instant of our identity class; id

	strcpy(id.name,"Paul N Muchene"); // Copy Name to instant name variable assignments are illegal 
	strcpy(id.address,"P.O. BOX 29121 Nairobi, Kenya"); // Copy Address to address variable

	id.age=28; //Assign instant age variable a value 28

	printf("Personal Details are:\n Name: %s\t Address: %s\t Age: %d\n",id.name,id.address,id.age

); // Print results to stdout

	return (0); // Function main returns a vaule
}
/* Program Ends */
