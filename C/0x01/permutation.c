/*

Title: permutation.c

Description: A program that reads in a positive integer at least equal to 3 and prints out all possible permutations of
three positive integers less or equal to than this value.

Compilation: cc -o permutation permutation.c

Author: Paul N Muchene

*/

#include <stdio.h> //Include standard I/O

int main()
{

	int number,permutation,loop,loop2; //integer,factorial and looping variables

	printf("\nEnter a positive integer:\n"); //prompt user to enter integer
	scanf("%d",&number); //read from stdout


	 if(number<3) /* Integer must be greater than 3*/
	{
		printf("\nEnter integer greater or equal to 3\n");
		return 0;
		
	}

	else /* Calculate for factorials greater than 1*/
	{

		loop=number; //for purposes of looping
	
		do
		{

			permutation=loop*(loop-1); // Initialise variable permutation

		
			for(loop2=loop-1; loop2>1; loop2--) /* factorial=numberx(number-1)! thus loop2=loop-1 */
			{
				permutation=permutation*(loop2-1); //Calculate factorial
		
			}
			
	
			if(loop==0||loop==1){permutation=1;} /* if number=0 or number=1 then factorial=1*/


			printf("%d factorial is: %d\n",loop,permutation); /* Display results*/


			loop--; //decrement loop 

		}while(loop>=number-3); /* loop for 3 consecutive integral values less than variable 'number' */

	}


	return 0; // Return from function


} /*End of program*/
