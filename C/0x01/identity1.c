/*

Title:	identity.c

Description: A struct program where a user inputs his/her name, address and age
		The result is printed out in stdout

Compilation: cc -o identitiy identity.c

Author: Paul N Muchene

*/

#include <stdio.h> //Standard input output
#include <string.h> //String library


/* Define a structure to hold name, address and age */
struct identity{  
	char name[100]; //Name
	char address[100]; //Address
	int age;//Age
};

struct  identity id; //Call an instant of our identity class; id

const  int MAX=100; // Maximum size of entries

void getname();

void getaddress();

int getage();

/* Function to obtain name from stdin*/ 
void getname ()
{

	int i=0;
	char ch,buffer[MAX];

	printf("\nPlease enter your name: \t");

	while ((ch!='\n') && (i<MAX))
	{
		ch=getchar();
		buffer[i++]=ch;
		
		if(ch=='\n')
		{
			strncpy(id.name,buffer,strlen(buffer));
			getaddress();	

		}

	}

}

/* Function to obtain address from stdin */
void getaddress ()
{

	int i=0;
	char ch,buffer2[MAX];

	printf("\nEnter your address: \t");

	while ((ch!='\n') && (i<MAX))
	{
		ch=getchar();
		buffer2[i++]=ch;
		
		if(ch=='\n')
		{
			strncpy(id.address,buffer2,strlen(buffer2));
			getage();	

		}

	}

}

/* Function to obtain age from stdin */
int  getage ()
{

	printf("\nWhat is your age: \t");

	scanf("%d",&id.age);

	return id.age;
}

/* Our program begins here*/
int main()
{

	getname();

	printf("\nPersonal Details are:\n\
		 Name: %s\t\n\
		 Address: %s\t\n\
		 Age: %d\t\n"\
		,id.name,id.address,id.age\
		);//print results to stdout

	return 0; // Function main returns a vaule

}/*Program ends */

