/*

Title: circle.c

Description: This program reads the radius from standard input and prints the area of a circle

Compilation: cc -o circle circle.c

Author: Paul N Muchene

*/

#include<stdio.h> // Standard input/output library
#include<math.h> // Math library

float radius; //radius declared as float
float area; // area declared as float
const float pi=3.14159; //value of pi

/* The program starts here*/
int main()
{
	printf("\n Enter radius of circle:\t"); //Prompt for radius
	scanf("%f",&radius); //Read value of radius
	
	if(radius<0) /* Check for negative values of radius*/
	{
		printf("\n Negative values disallowed\n\n");
		return 1; 
	}
	
	area=pi*(pow(radius,2)); //Calculate area of circle

	printf("\nThe area of your circle is:\t%.2f\n",area); //Display result of area

	return 0; //Return

}/*Program ends*/

