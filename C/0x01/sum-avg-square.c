/* Program that inpus 2 numbers and works out sum, average and sum of the squares 
*/

#include <stdio.h> //Standard I/O
#include <stdlib.h> //Standard lib
#include <math.h> //Math lib

/*Main program starts here */
int main()
{
	int x,y,sum,squaresum; // 2 input variables x,y , sum and sum of squares declared as int
	float average; // Mean as float

	printf("Enter 2 numbers:\n"); // Prompt the user to enter 2 numbers
	scanf("%d",&x); // Read the first number from stdin
	scanf("%d",&y); // Read the second number from stdin

	sum=x+y; // Find the sum of the 2 numbers 

	average= (x+y)/2.0; // Find the average of the 2 numbers

	squaresum= pow(x,2)+pow(y,2); // Find the sum of the squares of the 2 numbers

	printf("\nSum: %d Average: %.2f SquareSum: %d  \n",sum,average,squaresum); // Print results

	exit(0); // Call exit system call 
}
/* Program ends */
