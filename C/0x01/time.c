/*

Title: time.c

Description: This program takes an input of seconds of type integer and prints out the equivalent time in 
	     hours, minutes and seconds

Compilation: cc -o time time.c


Author: Paul N Muchene

*/

#include<stdio.h> //Standard i/o library
#include<math.h> //Math library

/* Main program starts here*/
int main()
{
	int time,hour,minute,second; //time variables in int
	
	printf("\nEnter your time in seconds:\t"); //Prompt for time in seconds

	scanf("%d",&time); //Read from stdout

	hour=time/3600;	//Convert seconds to hours

	minute=(time-(hour*3600))/60; //Convert seconds to minutes

	second=(time-(hour*3600))-(minute*60); //Convert seconds to seconds

	printf("\n%d seconds is equivalent to %d hours %d minutes %d seconds\n",time,hour,minute,second);

	return 0; //Function returns value

}/* Program ends*/
