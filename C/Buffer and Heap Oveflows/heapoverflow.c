/*

    Name: heapoverflow.c 

    Description: This is an example program demonstration the heap buffer overflow 

    Source: https://www.win.tue.nl/~aeb/linux/hh/hh-11.html

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    char *p, *q;

    p=malloc(sizeof(char));
    q=malloc(sizeof(char));

    if(argc>=2)
    {
        strcpy(p,argv[1]);
    }

    free(p);
    free(q);

    return 0;
}
