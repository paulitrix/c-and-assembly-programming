/*

Title: date.c

Description: This program reads in 3 integer values from stdin representing
	     day,month and year and it then prints out the following day's date

Compilation: cc -o date date.c

Author: Paul N Muchene

*/


#include<stdio.h> //Standard i/o library

#include<stdlib.h> //Standard utility library

#include<math.h> //Math library

#include<string.h> //String library 

void leapday(int day,int month, int year);

char buff[2],buff1[2];
char one[2],two[2];

void nextday(int day, int month, int year)
{

	switch( month)
	{
		case 2:
			if(day>=28)
			{		
				leapday(day,month,year);

			}

			else
			{
				day=day+1;
				
				sprintf(buff,"%d",month);
				strcpy(one,"0");
				strcat(one,buff);

				sprintf(buff1,"%d",day);
				strcpy(two,"0");
				strcat(two,buff1);

				printf("\n The following day is: %s:%s:%d\n",one,two,year);
			}
			

		break;
		
		case 12:

			/* if month is december*/	
			if(day==31)
			{
				day=1;
				month=1;
				year=year+1;
				
				sprintf(buff,"%d",month);
				strcpy(one,"0");
				strcat(one,buff);

				sprintf(buff1,"%d",day);
				strcpy(two,"0");
				strcat(two,buff1);
			}

			else
			{

				day=day+1;
				
				sprintf(one,"%d",month);
				sprintf(two,"%d",day);
			}
		
			printf("\n The following day is: %s:%s:%d\n",two,one,year);

		break;

		default:

				
			/* 6 months with 31 days except december*/
			if((month== 1||3||5||7||8||10)&& (day==31))
			{
				day=1;	
				month=month+1; 

			}

			/* 30 days have april,november,june and september */
			else if((month== 4||6||9||11)&& (day==30))
			{
				day=1;
				month=month+1;
			}

			else
			{
				day=day+1;
				if((month<10) || (day<10))
				{
					sprintf(buff,"%d",month);
					strcpy(one,"0");
					strcat(one,buff);

					sprintf(buff1,"%d",day);
					strcpy(two,"0");
					strcat(two,buff1);
				}

				else
				{
					sprintf(one,"%d",month);
					sprintf(two,"%d",day);
			
				}				
			}
			
				printf("\n The following day is: %s:%s:%d\n",two,one,year);


		break;

	}

}


void leapday(int day,int month,int year )
{

	int leapyear;

	leapyear=year%4;

	switch(leapyear)
	{
		
		case 0:
		{	

			if(day==28)
			{
				day=day+1;

				sprintf(buff,"%d",month);
				strcpy(one,"0");
				strcat(one,buff);

				sprintf(buff1,"%d",day);
				strcpy(two,"0");
				strcat(two,buff1);
				
			}
			
			if(day==29)
			{
				day=1;	
				month=month+1;
				
				sprintf(buff,"%d",month);
				strcpy(one,"0");
				strcat(one,buff);

				sprintf(buff1,"%d",day);
				strcpy(two,"0");
				strcat(two,buff1);
			
			}
			
			else if(day>29)
			{
				printf("\nNo such day!\n");
				exit(0);	
			}
		

		}
		break;

		default:
		{	
			if(day==28)
			{
				day=1;
				month=month+1;	

				sprintf(buff,"%d",month);
				strcpy(one,"0");
				strcat(one,buff);

				sprintf(buff1,"%d",day);
				strcpy(two,"0");
				strcat(two,buff1);

			}
			
			else
			{

				printf("\nNo such day!\n");
			}

			

		}	
		break;

	}

	printf("\n The following day is: %s:%s:%d\n",one,two,year);

}


/*Main program starts here*/
int  main()
{

	int day,month,year;

	printf("\n Enter day:\t");
	scanf("%d",&day);
	
	printf("\n Enter month:\t ");
	scanf("%d",&month);

	printf("\n Enter year:\t ");
	scanf("%d",&year);
	
	if(day>31 || month>12) 
	{
		printf("\n Incorrect day or month!\n");
		exit(0);
	}


	nextday(day,month,year);	
	

	return 0; //Function has to return value

}/*Program ends*/
