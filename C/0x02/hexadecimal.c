/*

Title: hexadecimal.c

Description: This program reads in 2 characters and prints their values as
	     a 2-digit hexadecimal number.

Compilation: cc -o hexadecimal hexadecimal.c 

Author: Paul N Muchene

*/

#include<stdio.h> //Standard i/o library
#include<string.h> //String library

/*
 This function reads the modulus of an integer with 16.

 Based on a criteria a character is returned by the function.

*/
char convert(int digit)
{
	char result;

	switch(digit)
	{
		case 0: //Modulus yields 0
		{
			 result='0';	
		}break;

		case 1: //Modulus yields 1
		{
			result='1';
		}break;

		case 2: //Modulus yields 2
		{
			result='2';
		}break;

		case 3: //Modulus yields 3
		{
			result='3';
		}break;

		case 4: //Modulus yields 4
		{
			result='4';
		}break;

		case 5: //Modulus yields 5
		{
			result='5';
		}break;

		case 6: //Modulus yields 6
		{
			result='6';
		}break;

		case 7: //Modulus yields 7
		{
			result='7';
		}break;

		case 8: //Modulus yields 8
		{	
			result='8';
		}break;

		case 9: //Modulus yields 9 
		{
			result='9';
		}break;

		case 10: //Modulus yields 10
		{
			result='A';
		}break;

		case 11: //Modulus yields 11
		{
			result='B';
		}break;

		case 12: //Modulus yields 12
		{
			result='C';
		}break;

		case 13: //Modulus yields 13
		{
			result='D';
		}break;

		case 14: //Modulus yields 14
		{
			result='E';
		}break;

		case 15: //Modulus yields 15
		{ 
			result='F';
		}break;
		
		default:
		{
		// Do nothing
		}break;
	}

	return result; //Return character 

}/* End of conversion function*/

/* Function responsible for converting decimal integers to hexadecimal

   First divide the integer by 16 also find the modulus of the integer
   
   If the division meets a certain criteria return a character for the 
	
   first digit.

   The second digit on the other  hand is passed to a conversion function.

*/
void hexadecimal(int temp, int temp2, char hex[2])
{
		switch(temp)
		{
			case 0: // Result of division is zero
			{
				hex[0]='0';
				hex[1]=convert(temp2); //Call convert
			}break;

			case 1: // Result of division is one
			{
				hex[0]='1';
				hex[1]=convert(temp2);
			}break;

			case 2: // Result of division is two
			{
				hex[0]='2';
				hex[1]=convert(temp2);
			}break;

			case 3: // Result of division is three
			{
				hex[0]='3';
				hex[1]=convert(temp2);
			}break;

			case 4: // Result of division is four
			{
				hex[0]='4';
				hex[1]=convert(temp2);
			}break;

			case 5: // Result of division is five
			{
				hex[0]='5';
				hex[1]=convert(temp2);
			}break;

			case 6: //Result of division is six
			{
				hex[0]='6';
				hex[1]=convert(temp2);
			}break;
		}

}/* End of function */

/*Program begins here*/
int main()
{
	int loop,num,temp,temp2; //Looping and temp variables
	char hex[2],hex1[2]; //Character arrays

	printf("Enter a 2 digit integer:\t"); //Prompt user for input

	for(loop=0; loop<2; loop++)
	{
		scanf("%c",&hex[loop]); //Read input of 2 chars from stdin

	}

	num=atoi(hex); //Convert values in char array to integer

	temp=num/16; //Find the value of integer as multiple of 16

	temp2=num%16; // Find modulus of integer in base 16

	hexadecimal(temp,temp2,hex); //Call on function for converting to hex

 	printf("\n%d as hexadecimal is: %s\n",num,strcpy(hex1,hex)); //Display

	return 0; //Program returns value

}/*Program ends*/
