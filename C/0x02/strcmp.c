/*

Title: strcmp.c

Description: This program reads in two strings from stdin giving the following:
		
		1. If first string is greater than second then print 'UP'
		
		2. If second string is greater than first then print 'Down'

		3. If both strings are equal print 'Error' then exit

Compilation: cc -o strcmp strcmp.c

Author: Paul N Muchene

*/

#include<stdio.h>  //Standard i/o library
#include<string.h> //String library
#include<stdlib.h> //Standard library

/* Main program starts here */
main()
{
	char buff[20],buff1[20];
	int compare,check,check1;
	
	printf("Enter your first string:\t");
	scanf("%s",&buff);

	printf("\nEnter your second string:\t");
	scanf("%s",&buff1);
	check=strlen(buff);
	check1=strlen(buff1);
	

	if(check>20)
	{
	 printf("\nError\n");
	 exit(0);
	}
	
	if(check1>20)
	{
	 printf("\nError\n");
	 exit(0);
	}

	else 
	{	
	compare=strcmp(buff,buff1);
	
	if(compare==0){ printf("\nEqual\n"); exit(0);}

	(compare>0) ?  printf("\nUp\n") : printf("\nDown\n");
	
	}

}/* Program ends */
