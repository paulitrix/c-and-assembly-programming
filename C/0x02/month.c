/*

Title: month.c

Descripton: This program reads an integer from 1-12 from stdin and prints
	    out the equivalent month as string

Compilation: cc -o month month.c

Author: Paul N Muchene

*/
#include<stdio.h> //Standard i/o library
#include<string.h> // String library
#include<stdlib.h> //Standard library

char * month(int number); //Function to convert integer to month 
char buff[20]; // Buffer to hold string

char * month(int number)
{

	switch(number)
	{
		case 1:
			strcpy(buff,"January");
		break;

		case 2:
			strcpy(buff,"February");
		break;

		case 3:
			strcpy(buff,"March");
		break;

		case 4:
			strcpy(buff,"April");
		break;

		case 5:
			strcpy(buff,"May");
		break;

		case 6:
			strcpy(buff,"June");
		break;

		case 7:
			strcpy(buff,"July");
		break;

		case 8:
			strcpy(buff,"August");
		break;

		case 9:
			strcpy(buff,"September");
		break;

		case 10:
			strcpy(buff,"October");
		break;

		case 11:
			strcpy(buff,"November");
		break;

		case 12:
			strcpy(buff,"December");
		break;

		default:
		 	printf("\nInteger must be between 1 and 12\n");
			exit(1);
		break;
	}
	
	return buff;

}

/* Main program*/
int main()
{
	int number;

	printf("\nEnter an integer:\t");
	scanf("%d",&number);

	month(number);

	printf("\nYour month is:\t%s\n\n",month(number));

	return 0;

}/* Program ends here*/
