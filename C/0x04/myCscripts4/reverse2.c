/*

Title: reverse2.c

Description: This program reads ordinary text a character at a time from standard input, and prints it with each line reversed from left to right. When End of data is encountered the program terminates

Compilation: cc -o reverse2 reverse2.c

Execution: ./reverse2 (From (ba|c|tc|sh) shell)

Author: Paul N Muchene

*/

#include <stdio.h> //I/O library 
#include <string.h> //String library
#include <stdlib.h> //Standard library
#include <ctype.h> //Character manipulation library 


void strrev(char string[],int count)
{

	int i,j;
	char ch,word[1000];

	for(i=0,j=count-2;i<count,j>=0; i++,j--)
	{	
		ch=string[i];
		word[j]=ch;
		
	}


	printf("Your reversed string is: %s\n",word);

}


int main()
{

	char ch, buff[1000];
	int loop=0,i; 

	printf("\nEnter your characters:\n");

	while(scanf("%c",&ch)==1)
	{
		buff[loop]=ch;
		loop++;

	}

	strcat(buff,"\0");

	strrev(buff,loop);

	return 0;
}
