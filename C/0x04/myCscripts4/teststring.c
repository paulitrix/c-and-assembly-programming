#include <stdio.h>
#include <ctype.h>
#include <string.h>

int main()
{
	char ch;

	printf ("\nEnter a character string:\t");
	scanf("%c",&ch);

	printf("\nResult:\t%d %d %d %d\n",isalpha(ch),isalnum(ch),iscntrl(ch),ispunct(ch));
	return 0; 
}
