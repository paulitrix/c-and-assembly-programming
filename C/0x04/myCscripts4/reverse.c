/*

Title: reverse.c

Description: This program reads ordinary text a character at a time from standard input, and prints it with each line reversed from left to right. When End of data is encountered the program terminates

Compilation: cc -o reverse reverse.c

Execution: ./reverse (From (ba|c|tc|sh) shell)

Author: Paul N Muchene

*/

#include <stdio.h> //I/O library 
#include <string.h> //String library
#include <stdlib.h> //Standard library
#include <ctype.h> //Character manipulation library 

/* Function to reverse a string of characters */
char * strrev(char *string)
{

	char temp, *i,*j;	

	i=&string[0]; //address of first character

	j=&string[strlen(string)-1]; //address of last character

	if(string==NULL||!*string) //If string is null or empty
	{
		printf("\nEmpty string\n");
		return ;
	}


	while(j>i) //swap
	{

		temp=*i;
		*i=*j;
		*j=temp;

		i++;
		j--;

	}	

	return string; //return pointer to reversed array
	
}


/* Main program starts here */
int main()
{

	char ch, buff[1024];
	int loop=0; 

	printf("\nEnter your characters:\n"); //Prompt user 

	while(ch=getchar(),ch>=0)
	{
		buff[loop]=ch;
		loop++;

	}

	printf("\n\nThe string before reversal:\n%s\n",buff);

	strrev(buff); //Call reverse function

	printf("\n\nThe string after reversal:\n%s\n",buff);

	return 0;

}/*Program ends*/
