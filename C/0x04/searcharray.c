/*

Description: This program performs the following:

	1. Read and scan through an array for a particular value
	2. Search an array of any type for a particular value

Compilation: cc -o searcharray searcharray.c

Author: Paul N Muchene


CAVEAT: MAKE SURE ALL YOUR STRINGS ARE NULL TERMINATED!!! OR ELSE :-( 

*/

#include <stdio.h>

#include <stdlib.h>

#include <string.h> 

/*Program starts here*/
int main()
{
	int ch,i=0,loop=0;
	char string[1000],buff[1000],search[1000],newstring[1000];
	const char *strx,*stry,*strz;

	printf("Enter values of any type:\n");

	do
	{
		ch=getchar();
		buff[loop]=ch; 
		loop++;

	}while(ch !='\n');

	strcat(buff,"\0");

	strncpy(string,buff,strlen(buff));

	printf("\nEnter a search string:\n");

	//scanf("%s",&search);

	do
	{
		ch=getchar();
		newstring[i]=ch; 
		i++;

	}while(ch !='\n');
	
	
	strcat(newstring,"\0");

	strncpy(search,newstring,strlen(newstring));


	strx=string;

	stry=search;

	strz=strstr(strx,stry);	

	if(strz)
	{

		printf("\n Search returns a match:\t %s \r for your query string:\t %s \n",strx,stry);
	
	}
	else
	{

		printf("\n Search did not return any match for your query \n ");
	}


	return 0;

}/*(End)*/
