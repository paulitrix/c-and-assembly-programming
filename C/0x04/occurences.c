/*
	Title: occurences.c
	
	Description: This program reads characters from standard input 
		     and outputs the word lengths and their occurences 

	Compilation: cc -o occurences occurences.c

	Execution: ./occurences  // {(ba|c|tc|)sh shell }

	Author: Paul N Muchene

*/

#include <stdio.h> //Standard i/o
#include <stdlib.h> //Standard lib
#include <string.h> //String lib
#include <ctype.h> //Char manipulation lib

/* 
	The occurence function takes in a pointer to an array of pointers to a string

*/
int checkstring(char *string)
{
	char *ch;
	int count0=0,count1=0,count2=0,count3=0,count4=0,count5=0,\
	    count6=0,count7=0,count8=0,count9=0,count10=0,count11=0,\
	    count12=0,count13=0,count14=0,count15=0,count16=0,count17=0,\
	    count18=0,count19=0,count20=0,count21=0,count22=0,count23=0,\
	    count24=0; //initalise 25 variables


	for(ch=strtok(string," ");ch!=NULL;ch=strtok(NULL," "))
	{

		/*
			Function breaks array to tokens and finds occurences
			of up to 25 characters 

		*/

		switch(strlen(ch))
		{
			case 1:
				count0++;
			break;
			
			case 2:
				count1++;
			break;

			case 3:
				count2++;
			break;
			
			case 4:
				count3++;
			break;
			
			case 5:
				count4++;
			break;

			case 6:
				count5++;
			break;

			case 7:
				count6++;
			break;

			case 8:
				count7++;
			break;

			case 9:
				count8++;
			break;

			case 10:
				count9++;
			break;

			case 11:
				count10++;
			break;

			case 12:
				count11++;
			break;

			case 13:
				count12++;
			break;

			case 14:
				count13++;
			break;

			case 15:
				count14++;
			break;

			case 16:
				count15++;
			break;

			case 17:
				count16++;
			break;

			case 18:
				count17++;
			break;

			case 19:
				count18++;
			break;

			case 20:
				count19++;
			break;

			case 21:
				count20++;
			break;

			case 22:
				count21++;
			break;

			case 23:
				count22++;
			break;

			case 24:
				count23++;
			break;

			default:
				
			break;
		}
			
	}	
		/* Display results to standard output */
		printf("\nLength 1: %d  occurrences\n",count0);
		printf("\nLength 2: %d  occurrences\n",count1);
		printf("\nLength 3: %d  occurrences\n",count2);
		printf("\nLength 4: %d  occurrences\n",count3);
		printf("\nLength 5: %d  occurrences\n",count4);
		printf("\nLength 6: %d  occurrences\n",count5);
		printf("\nLength 7: %d  occurrences\n",count6);
		printf("\nLength 8: %d  occurrences\n",count7);
		printf("\nLength 9: %d  occurrences\n",count8);
		printf("\nLength 10: %d  occurrences\n",count9);
		printf("\nLength 11: %d  occurrences\n",count10);
		printf("\nLength 12: %d  occurrences\n",count11);
		printf("\nLength 13: %d  occurrences\n",count12);
		printf("\nLength 14: %d  occurrences\n",count13);
		printf("\nLength 15: %d  occurrences\n",count14);
		printf("\nLength 16: %d  occurrences\n",count15);
		printf("\nLength 17: %d  occurrences\n",count16);
		printf("\nLength 18: %d  occurrences\n",count17);
		printf("\nLength 19: %d  occurrences\n",count18);
		printf("\nLength 20: %d  occurrences\n",count19);
		printf("\nLength 21: %d  occurrences\n",count20);
		printf("\nLength 22: %d  occurrences\n",count21);
		printf("\nLength 23: %d  occurrences\n",count22);
		printf("\nLength 24: %d  occurrences\n",count23);
		printf("\nLength 25: %d  occurrences\n\n",count24);

}

/* Main program starts here*/

int main()
{
	char ch,buff[512],*buff2[512],*string;
	int loop=0;

	printf("\nEnter a sequence of characters:\n"); //prompt for user input

	while(ch=getchar(),ch>=0)
	{
		buff[loop]=ch;
		string=&buff[loop];
		buff2[loop]=string;
		loop++;
	}
	

	checkstring(*buff2); //Call our function

	return 0; //Return

}/*End of program*/
