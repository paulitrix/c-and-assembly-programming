/*

Title: reverse.c

Description: This program reads ordinary text a character at a time from standard input, and prints it with each line reversed from left to right. When End of data is encountered the program terminates

Compilation: cc -o palindrome reverse.c

Execution: ./palindrome (From (ba|c|tc|sh) shell)

Author: Paul N Muchene

*/

#include <stdio.h> //I/O library 
#include <string.h> //String library
#include <stdlib.h> //Standard library
#include <ctype.h> //Character manipulation library 

/* Function to reverse a string of characters */
char * strrev(char *string)
{

	char temp, *i,*j;	

	i=&string[0]; //address of first character

	j=&string[strlen(string)-1]; //address of last character

	if(string==NULL||!*string) //If string is null or empty
	{
		printf("\nEmpty string\n");
		return ;
	}


	while(j>i) //swap
	{

		temp=*i;
		*i=*j;
		*j=temp;

		i++;
		j--;

	}	

	return string; //return pointer to reversed array
	
}

/* Palindrome function*/
int palindrome(char *string1)
{

	int compare;

	char *string2;

	string2=strrev(strdup(string1));

	compare=strcmp(string1,string2); //Call strrev function

	printf("Strings: %s  %s\n  Lengths: %d %d\n",string1,string2,strlen(string1),strlen(string2));

	free(string2);

	return compare;

}

/* Main program starts here */
int main()
{

	char ch, buff[1024];
	int loop=0; 

	printf("\nEnter your characters:\n"); //Prompt user 

	
	while(scanf("%c",&ch)==1)
	{
		buff[loop]=ch;
		loop++;
	}

	if(palindrome(buff)==0) //Call palindrome function
	{
		printf("\nThe string is a palindrome\n");
	}
	else
	{
		printf("\nThe string is not a palindrome\n");
	}

	return 0;

}/*Program ends*/
