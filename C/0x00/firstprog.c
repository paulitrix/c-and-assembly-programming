/*

Title: A simple program to display Hello Paul to Stdout

Author: Paul Muchene

Date: 31-08-2009

*/

#include <stdio.h>

int main()
{
	int i;

	for(i=0; i<10; ++i) //Loop 10 times before evaluating expression
	{
		puts("Hello Paul!\n"); //Display to stdout
		
	}

	return 0; //Exit cleanly 
}
