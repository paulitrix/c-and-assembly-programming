/*

Title: centinches.c

Description: This program takes in a given input in centimetres as a float
	     and outputs the result in feet and inches.

Compilation: cc -o centinches centinches.c

Author: Paul N Muchene

*/


#include<stdio.h> // Standard i/o library
#include<math.h> // Math library


/* Main program starts here*/
int main()
{

	float dimensions,workout,inches;
	const float conversion=2.54;
	int feet;

	printf("\n Enter your dimensions in centimetres:\t"); //Prompt for dimensions to convert

	scanf("%f",&dimensions); //read from stdout

	workout=dimensions/conversion; //Convert centimetres to inches

	inches=workout-(feet*12); //Extract the inches

	printf("\n %.2f centimeteres is %d feet and %.2f inches\n\n",dimensions,feet,inches); //Display results

	return 0; //Function returns a value

}/* End ofprogram */


